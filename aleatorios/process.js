const {createApp} = Vue;
createApp({
    data(){
        return {
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais
            imagensLocais:[
                './imagens/lua.jpg',
                './imagens/sol.jpg',
                './imagens/Senai_logo.png'
            ],
            imagensInternet:[
                'https://img.freepik.com/vetores-gratis/bela-casa_24877-50819.jpg?q=10&h=200',
                'https://upload.wikimedia.org/wikipedia/pt/5/58/Denver_Nuggets_logo.jpg',
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS54ALolaYi40EloMtjUr7qp5CVAUc3x3O_iA&usqp=CAU',

            ]

        };//Fim return
    },//Fim data

    computed:{
        randomImage()
        {
            return this.imagensLocais[this.randomIndex]
            
        },//Fim randomImage  

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndex]

            
        },//Fim randomInternet
    },//Fim computed

    methods:{
        getRandomImage()
        {
            this.randomIndex=Math.floor(Math.random()*this.imagensLocais.length)

            this.randomIndexInternet=Math.floor(Math.random()*this.imagensInternet.length)
        }
    },//Fim methods

}).mount("#app");