const { createApp } = Vue;

createApp({
  data() {
    return {
      numero: 200
    }
  },

    methods:{
        addValor:function(){
            this.numero++;
        }
    }
}).mount("#app");
